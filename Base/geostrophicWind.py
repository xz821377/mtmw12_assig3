# xz821377
"""
Created on Mon Oct 11 16:50:40 2021
MTMW12 Assignment 3 - More Complex Code
@author: penny
"""



# MTMW12 assignment 3. Hilary Weller 1 October 2020
# Python3 code to numerically differentiate the pressure in order to calculate 
# the geostrophic wind relation using 2-point differencing and
# compare with the analytic solution and plot

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def geostrophicWind():
    """ Calculate the geostrophic wind analytically and numerically and polt"""
    # Resolution and size of domain
    N = 10  # Numer of intervales to divide space into
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin) / N  # the length of the spacing
    
    # The spatial dimension, y:
    y = np.linspace(ymin, ymax, N+1)
    
    # The pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    # The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)
    
    Error10 = u_2point - uExact

#   print out some arrays to check against parallel excel model    
#   keep in to show I have done this!
#    print(u_2point)
#    print(dpdy)
#    print(uExact)
    
    # Graph to compare the numerical and analytic solutions
    # plot using large fonts
    font = {'size':14}
    plt.rc('font', **font)
    
    # plot the approximate and exact wind at y points
    plt.plot(y/1000, uExact, 'k-', label = 'Exact')
    plt.plot(y/1000, u_2point, '*k--', label = 'Two-point differences', \
        ms=12, markeredgewidth=1.5, markerfacecolor = 'none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindCent.pdf')
    plt.show()
    
 # plot the errors
    plt.plot(y/1000, u_2point - uExact, '*k--',\
    label = 'Two-point differences',\
             ms = 12, markeredgewidth = 1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.axhline(linestyle = '-', color = 'k')
    plt.xlabel('y (km)')
    plt.ylabel('u error (m/s)')
    plt.tight_layout()
    plt.savefig('geoWindErrorsCent.pdf')
    plt.show()
    
    np.savetxt('Error10.txt', Error10, fmt='%.8f')
    
if __name__ == "__main__":
   geostrophicWind()
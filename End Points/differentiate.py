# xz821377
"""
Created on Mon Oct 11 16:50:40 2021
MTMW12 Assignment 3 - More Complex Code
@author: penny
"""

import numpy as np

# Functions for calculating gradients

def gradient_2point(f, dx):
    """The gradient of one dimensional array f assuming points are a distance\
        dx apart using 2-point differences. Returns an array the same size as f"""
        
    # Initialised the array for the gradient to be the same size as f
    dfdx = np.zeros_like(f)
    # Two point differences at the end points
    dfdx[0] = (4*f[1]-3*f[0]-f[2])/(2*dx)
    dfdx[-1] = (3*f[-1]-4*f[-2]+f[-3])/(2*dx)
    # centered differences for the mid-points
    for i in range(1, len(f)-1):
        dfdx[i] = (f[i+1]-f[i-1])/(2*dx)
    return dfdx
    
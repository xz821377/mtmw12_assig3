# MTMW12_assig3

Python code for MTMW12 assignment 3 on Taylor expansions. 
Base direction contains the Taylor expansion with one-sided, two point 
finite dfference formula away from the end - points and using one-sided first 
order approximations at the end points

N=100 directions - as for base but with N=10 and additional code to ratio the errors

End Points - as for base but with improved approximations at the end points
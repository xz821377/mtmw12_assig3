# xz821377
"""
Created on Mon Oct 11 16:50:40 2021
MTMW12 Assignment 3 - More Complex Code
@author: penny
"""



# MTMW12 assignment 3. Hilary Weller 1 October 2020
# Python3 code to numerically differentiate the pressure in order to calculate 
# the geostrophic wind relation using 2-point differencing and
# compare with the analytic solution and plot

import numpy as np
import matplotlib.pyplot as plt
from differentiate import *
from physProps import *

def geostrophicWind10():
    """ Calculate the geostrophic wind analytically and numerically for N=10"""
    # Resolution and size of domain
    N = 10  # Numer of intervales to divide space into
    ymin = physProps["ymin"]
    ymax = physProps["ymax"]
    dy = (ymax - ymin) / N  # the length of the spacing
    
    # The spatial dimension, y:
    y = np.linspace(ymin, ymax, N+1)
    
    # The pressure at the y points and the exact geostrophic wind
    p = pressure(y, physProps)
    uExact = uGeoExact(y, physProps)
    
    # The pressure gradient and wind using two point differences
    dpdy = gradient_2point(p, dy)
    u_2point = geoWind(dpdy, physProps)

    # Calculate the errors    
    Error = u_2point - uExact
    
    
    
# Now repeat but with 10 times more intervals. i.e.N=100
    N100 = N*10  # Numer of intervales to divide space into
    dy100 = (ymax - ymin) / N100  # the length of the spacing
    
    # The spatial dimension, y:
    y100 = np.linspace(ymin, ymax, N100+1)
    
    # The pressure at the y points and the exact geostrophic wind
    p100 = pressure(y100, physProps)
    uExact100 = uGeoExact(y100, physProps)
    
    # The pressure gradient and wind using two point differences
    dpdy100 = gradient_2point(p100, dy100)
    u_2point100 = geoWind(dpdy100, physProps)
    
    
# create an array with errors at N = 0, 10, 20, ...., 100
    Error100 = np.zeros(11)

#populate the arrays
    for n in np.arange(0,11):
        Error100[n] = u_2point100[10*n]-uExact100[10*n]
 

    
# Ratio the errors:
    RatioofErrors = Error100 / Error
    print(RatioofErrors)
    
#Plot the ratio of the errors:

    plt.plot(y/1000, RatioofErrors, '*k--',\
    label = 'Errors for N=100 / Error for N=10',\
             ms = 12, markeredgewidth = 1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.axhline(linestyle = '-', color = 'k')
    plt.xlabel('y (km)')
    plt.ylabel('ratio')
    plt.tight_layout()
    plt.show()    
    
    
 
if __name__ == "__main__":
   geostrophicWind10()
  
   

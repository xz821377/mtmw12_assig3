# xz821377
"""
Created on Mon Oct 11 16:50:40 2021
MTMW12 Assignment 3 - More Complex Code
@author: penny
"""


# Physical properties and functions for calculating the geostropic wind

import numpy as np

# A disctionary of physical properties
physProps = {'pa' : 1e5,    # the mean pressure
             'pb' : 200,    # the magnitude of the pressure variations
             'f'  : 1e-4,   # the Coriolis parameter
             'rho': 1.,     # density
             'L'  : 2.4e6,  # length scale of the pressure variations (k)
             'ymin': 0.,  # start of the y domain
             'ymax': 1e6}   # end of the y domain

def pressure (y, props):
    """The pressure and given y locations based on dictionary of physical\
        properties, props"""
    pa = props["pa"]
    pb = props["pb"]
    L = props["L"]
    return pa + pb * np.cos(y*np.pi/L)

def uGeoExact(y, props):
    """The analystic geostrophic wind at given locations, y, based on \
        dictionary of physical properties, props"""
    pb = props["pb"]                
    L = props["L"]  
    rho = props["rho"]
    f = props["f"]
        
    return pb * np.pi / (rho*f*L)*np.sin(y*np.pi/L)
    
def geoWind(dpdy, props):
    """The geostrophic wind as a function of pressure gradiet based on the \
        dictionary of physical properties, props"""
    rho = props["rho"]
    f = props["f"]
    return -dpdy/(rho*f)
    
